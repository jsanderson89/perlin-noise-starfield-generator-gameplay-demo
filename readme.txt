Perlin Noise Starfield Generator - Game Prototype Version


This is an implementation of the initial Starfield Generator methods,
using them to create a scrolling background in an Asteroids style game.

A simplified version of the Starfield Generator is also included, and is
accessable through the menu option "Map Generation Demo."


Controls (Gameplay Demo):
	-Space = Shoot
	-Left/Right arrow = Rotate Ship
	-Up arrow - Accelerate ship forwards
	-Down arrow = Accelerate ship backwards

Issues:
	-The map generation before loading the "Gameplay Demo" is
	 very slow. Please allow a few moments for it to load.
	-The shots are not centered to the tip of the ship.


Copyright Jesse Anderson, 2013,2014.