package Classes.Actors 
{
	import BaseClasses.Utilities.Colors;
	import flash.display.Sprite;
	import flash.events.Event;
	import Classes.MapGeneration.BitmapProcessorController;
	import Classes.MapGeneration.CellDisplayer;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	
	/**
	 * ...
	 * @author a
	 */
	public class BackgroundSpace extends Sprite 
	{
		private var _height:int = 800;
		private var _width:int = 800;
		private var _blockSize:int = 100; 
		
		private var myBitmapDataObject:BitmapData;
		private var bitmap:BitmapProcessorController;
		private var myBitmap:Bitmap;
		
		private var progressBar:Sprite; 
		
		private var mapHolder:Sprite;
		private var cellArray:Array;
		private var display:CellDisplayer;
		private var totalCellsToDraw:int;
		private var totalCellsDrawn:int;
		
		public function BackgroundSpace(blockSize:int = 100, height:int = 800, width:int = 600, color:uint = 0x000000)
		{
			_blockSize = blockSize;
			_height = height;
			_width = width;
			var bgBuffer:int = 700;
			
			/*
			var bgSprite:Sprite = new Sprite();
			bgSprite.graphics.beginFill(color);
			bgSprite.graphics.drawRect(0, 0, height + bgBuffer, width + bgBuffer);
			bgSprite.graphics.endFill();
			bgSprite.x -= bgBuffer / 2;
			bgSprite.y -= bgBuffer / 2;
			addChild(bgSprite);
			*/
			
			totalCellsToDraw = (_height / blockSize) * (_width / blockSize);
			
			progressBar = new Sprite();
			progressBar.graphics.beginFill(Colors.WHITE);
			progressBar.graphics.drawRect(0, 0, 100, 30);
			progressBar.graphics.endFill();
			progressBar.y = 550
			addChild(progressBar);
			
			
			mapHolder = new Sprite();
			
			createBitmap();
			createBitmapHandler();
			
			bitmap.addEventListener("square drawn", progressBarTick);
		}
		
		private function progressBarTick(e:Event):void
		{
			totalCellsDrawn++;
			progressBar.scaleX = (totalCellsDrawn/totalCellsToDraw);
			
			if (totalCellsDrawn == totalCellsToDraw)
			{
				bitmap.removeEventListener("square drawn", progressBarTick);
				removeChild(progressBar);
			}
		}
		
		private function createBitmap():void
		{
			myBitmapDataObject = new BitmapData(_height, _width);
			var seed:Number = Math.floor(Math.random() * 100);
			myBitmapDataObject.perlinNoise(200, 200, 1, seed, true, true, 7, true, null);
			myBitmap = new Bitmap(myBitmapDataObject);
			mapHolder.addChild(myBitmap);
		}
		
		private function createBitmapHandler():void
		{
			bitmap = new BitmapProcessorController(myBitmapDataObject, _blockSize);
			bitmap.addEventListener("squares completed", createCells);
			mapHolder.addChild(bitmap);
			cellArray = bitmap.cellArray;
		}
		
		private function createCells(e:Event):void
		{
			bitmap.removeEventListener("squares completed", createCells);
			display = new CellDisplayer(cellArray);
			mapHolder.addChild(display);
			
			mapHolder.removeChild(bitmap);
			mapHolder.removeChild(myBitmap);
			
			dispatchEvent(new Event("bgSpace Complete"));
			addChild(mapHolder);
		}
	}

}