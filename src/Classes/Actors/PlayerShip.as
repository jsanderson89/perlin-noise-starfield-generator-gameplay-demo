package Classes.Actors 
{
	import BaseClasses.Actors.Actor;
	import BaseClasses.Utilities.Colors;
	import BaseClasses.Utilities.MathFunctions;
	import BaseClasses.Utilities.Vector2D;
	import flash.display.Sprite;
	
	/**
	 * ...
	 * @author a
	 */
	public class PlayerShip extends Actor 
	{
		public var rotationDegrees:Number;
		public var _speed:Number;
		private var _direction:Number = 0;
		private var _acceleration:Number = 5;
		public var friction:Number = .95;
		private var maxSpeed:Number = 15;
		private var triangleSize:Number = 20;
		private var displayImage:Sprite;
		private var nextVector:Vector2D;
		public var currentVector:Vector2D;
		
		public var xLoc:Number;
		public var yLoc:Number;
		
		public function PlayerShip() 
		{
			rotationDegrees = this.rotation;
			_speed = 0;
			xLoc = this.x;
			yLoc = this.y;
			draw();
		}
		
		override public function draw():void
		{
			displayImage = new Sprite();
			displayImage.graphics.beginFill(Colors.WHITE);
			displayImage.graphics.moveTo(triangleSize/2, 0);
			displayImage.graphics.lineTo(triangleSize, triangleSize);
			displayImage.graphics.lineTo( 0, triangleSize);
			displayImage.graphics.lineTo(triangleSize/2, 0);
			displayImage.graphics.endFill();
			addChild(displayImage);
			centerGraphic(displayImage);
		}
		
		override public function update():void
		{
			this.rotation = rotationDegrees;
			if (_speed >= maxSpeed)
			{
				_speed = maxSpeed;
			}
			nextVector = drive();
			currentVector = nextVector;
			xLoc += nextVector._x;
			yLoc += nextVector._y;
		}
		
		
		private function drive():Vector2D
		{
			var dy:Number = _speed * Math.cos(rotationAngle);
			var dx:Number = _speed * Math.sin(rotationAngle);
			var returnVector:Vector2D = new Vector2D(-dx, dy);
			//trace(dx, dy);
			return returnVector;
		}
		
		public function get acceleration():Number
		{
			return _acceleration;
		}
		
		public function get rotationAngle():Number
		{
			return this.rotationDegrees * Math.PI / 180;
		}
	}
}
