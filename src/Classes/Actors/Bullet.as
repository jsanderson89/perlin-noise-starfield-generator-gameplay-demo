package Classes.Actors 
{
	import BaseClasses.Actors.Actor;
	import BaseClasses.Utilities.Vector2D;
	import flash.display.Sprite;
	import BaseClasses.Utilities.Colors;
	
	/**
	 * ...
	 * @author a
	 */
	public class Bullet extends Actor 
	{
		private var displayImage:Sprite;
		private var _path:Vector2D;
		private var _angle:Number;
		private var _shotSpeed:Number = 15;
		
		public function Bullet(angle:Number) 
		{
			_angle = angle;
			draw();
		}
		
		override public function draw():void
		{
			trace("draw called" + " " + this.x + " " + this.y);
			displayImage = new Sprite();
			displayImage.graphics.beginFill(Colors.RED);
			displayImage.graphics.drawCircle(0, 0, 5);
			displayImage.graphics.endFill();
			addChild(displayImage);
		}
		
		override public function update():void
		{
			_path = drive();
			this.centerX += _path._x;
			this.centerY += _path._y;
		}
		
			
		private function drive():Vector2D
		{
			var dy:Number = _shotSpeed * Math.cos(rotationAngle);
			var dx:Number = _shotSpeed * Math.sin(rotationAngle);
			var returnVector:Vector2D = new Vector2D(dx, -dy);
			return returnVector;
		}
		
		public function get rotationAngle():Number
		{
			return this._angle * Math.PI / 180;
		}
	}

}