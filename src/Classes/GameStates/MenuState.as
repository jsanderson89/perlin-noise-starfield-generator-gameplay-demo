package Classes.GameStates 
{
	import BaseClasses.Utilities.Colors;
	import Classes.GameState;
	import flash.display.Sprite;
	import flash.events.Event
	import Classes.Events.ChangeState;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.text.TextFormat;
	
	/**
	 * ...
	 * @author a
	 */
	public class MenuState extends GameState 
	{
		private var sprite:Sprite;
		private var spriteTwo:Sprite;
		private var nextMenuOption:TextField;
		private var playMenuOption:TextField;
		[Embed(source="/Assets/Helvetica.ttf", fontName = 'Helvetica', mimeType = 'application/x-font-truetype', embedAsCFF = 'false')]
		private var HelveticaFont:Class;
		
		public function MenuState() 
		{
			if (this.stage) init()
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		public function init(e:Event = null):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			//entry point
			trace("MenuState entered");
			//dispatchEvent(new Event("pass event"));
			setStageFocus();
			drawMenu();
			
			sprite.addEventListener(MouseEvent.CLICK, onMouseClick);
			spriteTwo.addEventListener(MouseEvent.CLICK, onMouseClickTwo);
			
			dispatchEvent(new Event("init complete"));
		}
		
		private function drawMenu():void
		{
			nextMenuOption = new TextField();
			nextMenuOption.selectable = false;
			nextMenuOption.embedFonts = true;
			nextMenuOption.textColor = Colors.WHITE;
			nextMenuOption.autoSize = "left";
			nextMenuOption.defaultTextFormat = new TextFormat('Helvetica', 40);
			nextMenuOption.text = "Map Generation Demo";
			sprite = new Sprite();
			addChild(sprite);
			sprite.addChild(nextMenuOption);
			sprite.buttonMode = true;
			
			playMenuOption = new TextField();
			playMenuOption.selectable = false;
			playMenuOption.embedFonts = true;
			playMenuOption.textColor = Colors.WHITE;
			playMenuOption.autoSize = "left";
			playMenuOption.defaultTextFormat = new TextFormat('Helvetica', 40);
			playMenuOption.text = "Gameplay Demo";
			playMenuOption.y = sprite.height;
			spriteTwo = new Sprite();
			addChild(spriteTwo);
			spriteTwo.addChild(playMenuOption);
			sprite.buttonMode = true;
		}
		
		private function onMouseClick(e:MouseEvent):void
		{
			dispatchEvent(new ChangeState(ChangeState.DEFAULT, new MapGeneratorState()));
		}
		
		private function onMouseClickTwo(e:MouseEvent):void
		{
			dispatchEvent(new ChangeState(ChangeState.DEFAULT, new PlayState()));
		}
	}

}