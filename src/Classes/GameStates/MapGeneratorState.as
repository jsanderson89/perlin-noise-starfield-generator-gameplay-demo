package Classes.GameStates 
{
	import BaseClasses.Utilities.Keycodes;
	import Classes.GameState;
	import flash.display.BitmapData;
	import flash.display.Bitmap;
	import Classes.MapGeneration.BitmapProcessorController;
	import flash.events.Event;
	import Classes.MapGeneration.CellDisplayer;
	import flash.events.KeyboardEvent;
	import Classes.Events.ChangeState;
	
	/**
	 * ...
	 * @author a
	 */
	public class MapGeneratorState extends GameState 
	{
		
		private var myBitmapDataObject:BitmapData;
		private var bitmap:BitmapProcessorController;
		private var myBitmap:Bitmap;
		public var blockSize:int = 100; 
		public var cellArray:Array;
		public var display:CellDisplayer;
		public var isKeypressLocked:Boolean = false;
		
		public function MapGeneratorState() 
		{
			if (this.stage) init()
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		public function init(e:Event = null):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			//entry point
			trace("MapGenereratorState Entered");
			setStageFocus();
			GenerateMap();
			_stateStage.addEventListener(KeyboardEvent.KEY_UP, resetState);
			dispatchEvent(new Event("init complete"));
		}
		
		private function GenerateMap():void
		{
			isKeypressLocked = true;
			createBitmap();
			createBitmapHandler();
		}
		
		private function createBitmap():void
		{
			myBitmapDataObject = new BitmapData(800, 600);
			var seed:Number = Math.floor(Math.random() * 100);
			myBitmapDataObject.perlinNoise(200, 200, 1, seed, true, true, 7, true, null);
			myBitmap = new Bitmap(myBitmapDataObject);
			addChild(myBitmap);
		}
		
		private function createBitmapHandler():void
		{
			bitmap = new BitmapProcessorController(myBitmapDataObject, blockSize);
			bitmap.addEventListener("squares completed", createCells);
			addChild(bitmap);
			cellArray = bitmap.cellArray;
		}
		
		private function createCells(e:Event):void
		{
			bitmap.removeEventListener("squares completed", createCells);
			trace("makeCells");
			display = new CellDisplayer(cellArray);
			addChild(display);
			isKeypressLocked = false;
		}
		
		override public function destroy():void
		{
				//removeChild(bitmap);
				//removeChild(myBitmap);
				//removeChild(display);
		}
		
		private function resetState(e:KeyboardEvent):void
		{
			trace(isKeypressLocked);
			if (!isKeypressLocked)
				{
					if (e.keyCode == Keycodes.BACKSPACE_KEY)
					{
						dispatchEvent(new ChangeState(ChangeState.DEFAULT, new MenuState()));
					}
					
					if (e.keyCode == Keycodes.S_KEY)
					{
						dispatchEvent(new ChangeState(ChangeState.DEFAULT, new MapGeneratorState()));
					}
				}
		}
	}
}