package Classes.GameStates 
{
	import BaseClasses.Utilities.Keycodes;
	import Classes.Actors.BackgroundSpace;
	import Classes.Actors.Bullet;
	import Classes.Actors.PlayerShip;
	import Classes.Events.ChangeState;
	import Classes.GameCamera;
	import Classes.GameState;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	
	/**
	 * ...
	 * @author a
	 */
	public class PlayState extends GameState 
	{
		
	
		private var upKey:Boolean = false;
		private var downKey:Boolean = false;
		private var leftKey:Boolean = false;
		private var rightKey:Boolean = false;
		
		private var bullets:Vector.<Bullet>;
		private var cam:GameCamera;
		private var playerActor:PlayerShip;
		private var background:BackgroundSpace;
		private var keydown:Boolean = false;
		
		public function PlayState() 
		{
			if (this.stage) init()
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		public function init(e:Event = null):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			//entry point
			trace("PlayState Entered");
			
			setStageFocus();
			
			background = new BackgroundSpace(50, 2880, 2880);
			//background = new BackgroundSpace(100, 800, 600);
			background.addEventListener("bgSpace Complete", init_drawScene);
			addChild(background);
		}
		
		private function init_drawScene(e:Event):void
		{
			trace("draw");
			background.removeEventListener("bgSpace Complete", init_drawScene);
			playerActor = new PlayerShip();
			addChild(playerActor);
			cam = new GameCamera(background);
			playerActor.centerX = _stateStage.stageWidth / 2;
			playerActor.centerY = _stateStage.stageHeight / 2;
			
			bullets = new Vector.<Bullet>;
			
			_stateStage.addEventListener(KeyboardEvent.KEY_DOWN, onKeyPress);
			_stateStage.addEventListener(KeyboardEvent.KEY_UP, onKeyUp);
			dispatchEvent(new Event("init complete"));
		}
		
		override public function update():void
		{
			checkForMotion();
			playerActor.update();
			cam.moveCameraTo(playerActor.xLoc, playerActor.yLoc, 0);
			
			if (bullets.length >= 1)
			{
				for(var i:int = bullets.length-1; i >= 0; i--)
				{
					var targetBullet:Bullet = bullets[i];
  					targetBullet.update();
				}
			}
		}
		
		private function createShot():void
		{
			//if (bullets)
			//{
				//if (bullets.length < 5)
				//{
					var shot:Bullet = new Bullet(playerActor.rotation);
					shot.x = playerActor.centerX;
					shot.y = playerActor.centerY;
					addChild(shot);
					
					bullets.push(shot);
				//}
			
				//else
				//{
					//var targetBullet:Bullet = bullets[bullets.length-1];
					//bullets.pop();
					//bullets.push(shot);
				//}
			//}		
		}
		
		private function checkForMotion():void
		{
			if (upKey)
			{
				playerActor._speed ++;
			}
			
			else if (downKey)
			{
				playerActor._speed --;
			}
			
			else
			{
				playerActor._speed *= playerActor.friction;
			}
			
			if (leftKey)
			{
				//trace("left true");
				playerActor.rotationDegrees -= 5;
			}
			
			if (rightKey)
			{
				//trace("right true");
				playerActor.rotationDegrees += 5;
			}
		}
		
		private function onKeyPress(e:KeyboardEvent):void
		{
			switch (e.keyCode)
			{
				case Keycodes.UP_KEY:
					upKey = true;
					break;
				case Keycodes.DOWN_KEY:
					downKey = true;
					break;
				case Keycodes.LEFT_KEY:
					leftKey = true;
					break;
				case Keycodes.RIGHT_KEY:
					rightKey = true;
					break;
			}
		}
		
		private function onKeyUp(e:KeyboardEvent):void
		{
			switch (e.keyCode)
			{
				case Keycodes.UP_KEY:
					upKey = false;
					break;
				case Keycodes.DOWN_KEY:
					downKey = false;
					break;
				case Keycodes.LEFT_KEY:
					leftKey = false;
					break;
				case Keycodes.RIGHT_KEY:
					rightKey = false;
					break;
				case Keycodes.BACKSPACE_KEY:
					dispatchEvent(new ChangeState(ChangeState.DEFAULT, new MenuState()));
					break;
				case Keycodes.SPACE_KEY:
					createShot();
					break;
			}
		}
		
	}

}