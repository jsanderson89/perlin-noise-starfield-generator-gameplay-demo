package Classes 
{
	import flash.display.Sprite;
	import flash.display.Stage;
	
	/**
	 * ...
	 * @author a
	 */
	public class GameState extends Sprite 
	{
		protected var _stateStage:Stage;
		protected var _nextState:GameState;
		
		public function GameState() 
		{
			
		}
		
		public function update():void
		{
			
		}
		
		public function destroy():void
		{
			
		}
		
		public function get nextState():GameState
		{
			return _nextState;
		}
		
		public function set stateStage(i:Stage):void
		{
			_stateStage = i;
		}
		
		public function get stateStage():Stage
		{
			return _stateStage;
		}
		
		protected function setStageFocus():void
		{
			_stateStage.focus = stage;
		}
		
	}

}