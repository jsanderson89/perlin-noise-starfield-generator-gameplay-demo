package Classes.MapGeneration 
{
	import flash.display.BitmapData;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import Classes.MapGeneration.Block;
	/**
	 * ...
	 * @author a
	 */
	public class BitmapProcessorController extends Sprite
	{
		
		private var _data:BitmapData;
		private var _blockSize:int;
		private var columnPosition:int = 0;
		private var rowPosition:int = 0;
		private var xPos:int = 0; 
		private var yPos:int = 0;
		public var proccessComplete:Boolean = false;
		public var cellArray:Array;
		public var count:int;
		
		public function BitmapProcessorController(data:BitmapData, blockSize:int) 
		{
			_data = data;
			_blockSize = blockSize;
			
			cellArray = new Array();
			
 			//trace("Generating " + _data.width / _blockSize + " block columns and " + _data.height / _blockSize + " block rows.");
			
			addEventListener(Event.ENTER_FRAME, step);
		}
		
		private function step(e:Event):void
		{
			
			if (proccessComplete == true)
			{
				//trace("!");
				dispatchEvent(new Event("squares completed"));
				removeEventListener(Event.ENTER_FRAME, step);
			}
			
			else
			{
				//trace("...");
				processImageStep();
			}
			
		}
		
		private function processImageStep():void
		{
 			if (yPos < _data.height)
			{
				if (xPos < _data.width)
				{
					rowStep()
				}
			
				else
				{
					yPos += _blockSize;
					xPos = 0;
				}
			}
			
			else
			{
				//trace (cellArray);
				proccessComplete = true;
			}
		}
		
		private function rowStep():void
		{
			count++;
			//trace(count);
			var block:Block = getColorAverage();
			block.xVar = xPos;
			block.yVar = yPos;
			//trace("Drawing Block (" + block.color + ", " + block.xVar + ", " + block.yVar + ")");
			drawBlock(block, _blockSize);
			xPos += _blockSize;
			rowPosition += _blockSize;
		}
		
		private function getColorAverage():Block
		{
			var returnBlock:Block;
			var red:Number = 0;
			var green:Number = 0;
			var blue:Number = 0;
			
			var xLocation:int = xPos;
			var yLocation:int = yPos;
			
			var count:Number = 0;
			var pixel:Number;
			
			var outputColor:uint;
			
			for (var i:int = 0; i <= _blockSize; i++)
			{
				for (var j:int = 0; j <= _blockSize; j++)
				{
					pixel = _data.getPixel(xLocation, yLocation);
					red += pixel >> 16 & 0xFF;
					green += pixel >> 8 & 0xFF;
					blue += pixel & 0xFF;
					
					//trace("getColorAverage Location " + xLocation + ", " + yLocation);
					
					count++;
					yLocation++;
					if (yLocation > _blockSize)
					{
						yLocation = yPos;
					}
					
				}
				xLocation++
			}
			
			red /= count;
			green /= count;
			blue /= count;
			
			outputColor =  red << 16 | green << 8 | blue;
			//trace("working - getColorAverage(" + outputColor + ")");
			returnBlock = new Block(outputColor, xPos, yPos);
			return returnBlock;
		}
		
		private function drawBlock(inputBlock:Block, blockSize:int):void
		{
			//trace("drawblock on");
			var sprite:Sprite = new Sprite();
			//trace(xPos)
			//trace(yPos);
			sprite.graphics.beginFill(inputBlock.color);
			sprite.graphics.drawRect(0, 0, blockSize, blockSize);
			sprite.graphics.endFill();
			addChild(sprite);
			sprite.x = inputBlock.xVar;
			sprite.y = inputBlock.yVar;
			cellArray.push(new Cell(inputBlock.xVar, inputBlock.yVar, _blockSize, inputBlock.color));
			dispatchEvent(new Event("square drawn"));
		}
	}

}