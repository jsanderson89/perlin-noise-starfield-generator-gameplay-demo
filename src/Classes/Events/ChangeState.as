package Classes.Events
{
	import Classes.GameState;
	import flash.events.Event;
	
	/**
	 * ...
	 * @author a
	 */
	public class ChangeState extends Event 
	{
		
		public static const DEFAULT:String = "default";
		public static const ON_CHANGE_STATE:String = "onChangeState";
		public var _param:GameState;
		
		public function ChangeState(type:String, state:GameState, bubbles:Boolean=false, cancelable:Boolean=false) 
		{
			super(type, bubbles, cancelable);
			_param = state;
		}
		
		override public function clone():Event
		{
			return new ChangeState(type, _param, bubbles, cancelable);
		}
		
	}

}