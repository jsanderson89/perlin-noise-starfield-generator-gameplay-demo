package Classes 
{
	import flash.display.Sprite;
	import flash.geom.Rectangle;
	/**
	 * ...
	 * @author a
	 */
	public class GameCamera 
	{
		private var _stage:Sprite;
		private var _bounds:Rectangle;
		
		public function GameCamera(cameraStage:Sprite, bounds:Rectangle = null)
		{
			if (bounds == null)
			{
				bounds = new Rectangle(0, 0, 800, 600);
			}
			_stage = cameraStage;
			_bounds = bounds;
		}
		
		public function moveCameraTo(x:Number, y:Number, zoom:Number):void
		{
			var xZoom:Number = x * zoom;
			var yZoom:Number = y * zoom;
			
			_stage.x = x - (_stage.stage.stageWidth / 2);
			_stage.y = y - (_stage.stage.stageHeight / 2);
		}
		
	}

}