package  
{
	import BaseClasses.Game;
	import Classes.GameState;
	import flash.display.Sprite;
	import flash.display.Stage;
	import Classes.Events.ChangeState;
	import flash.events.Event;
	
	/**
	 * ...
	 * @author a
	 */
	public class GamePrototype extends Game
	{
		
		private var _gameStage:Stage;
		
		public var _currentState:GameState;
		public var _nextState:GameState;
		private var flagForStatechange:Boolean = false;
		private var init:Boolean = false;
		
		public function GamePrototype(gameStage:Stage, initialState:GameState) 
		{
			
			_gameStage = gameStage;
			changeState(initialState);
			_gameStage.addEventListener(Event.ENTER_FRAME, callUpdate);
		}
		
	private function callUpdate(e:Event):void
	{
		if (!flagForStatechange && init)
		{
			update();
		}
		
		else
		{
			changeState(_nextState);
		}
	}
	
	override public function update():void
	{
		_currentState.update();
	}
		
	public function get currentState():GameState
		{
			return _currentState;
		}
		
		public function set currentState(state:GameState):void
		{
			_currentState = state;	
		}
		
		public function get nextState():GameState
		{
			return _nextState;
		}
		
		public function set nextState(state:GameState):void
		{
			_nextState = state;
		}
		
		public function changeState(nextState:GameState):void
		{
			//trace(_currentState);
			flagForStatechange = false;
			init = false;
			if (_currentState)
			{
				_currentState.destroy();
				removeChild(_currentState)
				_currentState.removeEventListener(ChangeState.DEFAULT, onStateChange);
			}
			
			nextState.stateStage = _gameStage;
			_currentState = nextState;
			addChild(_currentState);
			_currentState.addEventListener(ChangeState.DEFAULT, onStateChange);
			_currentState.addEventListener("init complete", onInit);
		}
		
		private function onStateChange(e:ChangeState):void
		{
			_nextState = e._param;
			flagForStatechange = true;
		}
		
		private function onInit(e:Event):void
		{
			init = true;
			_currentState.removeEventListener("init complete", onInit);
		}
	}

}