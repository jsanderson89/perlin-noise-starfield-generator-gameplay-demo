package BaseClasses.Utilities 
{
	
	/**
	 * ...
	 * @author a
	 */
	public class MathFunctions 
	{
		
		public function MathFunctions() 
		{
			
		}
		
		public function degreesToRadians(degrees:Number):Number
		{
			return degrees * Math.PI / 180;
		}
		
		public function normalizeVector2D(angle:Number):Vector2D
		{
			return Vector2D(Math.cos(angle), Math.sin(angle));
		}
	}

}