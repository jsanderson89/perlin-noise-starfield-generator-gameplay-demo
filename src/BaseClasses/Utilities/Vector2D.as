package BaseClasses.Utilities 
{
	/**
	 * ...
	 * @author a
	 */
	public class Vector2D 
	{
		public var _x:Number;
		public var _y:Number;
		
		public function Vector2D(x:Number, y:Number)
		{
			_x = x;
			_y = y;
		}
		
	}

}