package BaseClasses.Utilities
{
	/**
	 * ...
	 * @author j
	 */
	public class Keycodes 
	{
		public static var SPACE_KEY:int = 32;
		public static var ENTER_KEY:int = 13;
		public static var BACKSPACE_KEY:int = 8;
		public static var UP_KEY:int = 38;
		public static var DOWN_KEY:int = 40;
		public static var RIGHT_KEY:int = 39;
		public static var LEFT_KEY:int = 37;
		public static var W_KEY:int = 87;
		public static var S_KEY:int = 83;
		
		public function Keycodes() 
		{

		}
		
	}

}